import React, { useState, useCallback } from 'react'
import styled from 'styled-components'
import { Card, CardBody, Button, Text } from '@toxicfinance/uikit'
import { useWallet } from '@binance-chain/bsc-use-wallet'
import useI18n from 'hooks/useI18n'
import { useAllHarvest } from 'hooks/useHarvest'
import useFarmsWithBalance from 'hooks/useFarmsWithBalance'
import UnlockButton from 'components/UnlockButton'
import ToxicHarvestBalance from './ToxicHarvestBalance'
import ToxicWalletBalance from './ToxicWalletBalance'

const StyledFarmStakingCard = styled(Card)`
  background-size: 256px;
  background-repeat: no-repeat;
  background-position: top right;
  min-height: 376px;
`

const Block = styled.div`
  margin-bottom: 16px;
`

const TokenImageWrapper = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 16px;
`

const CardImage = styled.img`
  margin-right: 8px;
`

const Label = styled.div`
  color: ${({ theme }) => theme.colors.textSubtle};
  font-size: 18px;
`

const Actions = styled.div`
  margin-top: 24px;
`

const Divider = styled.div`
  background-color: ${({ theme }) => theme.colors.textSubtle};
  height: 1px;
  width: 100%;
  margin: 24px 0 24px 0;
`

const FarmedStakingCard = () => {
  const [pendingTx, setPendingTx] = useState(false)
  const { account } = useWallet()
  const TranslateString = useI18n()
  const farmsWithBalance = useFarmsWithBalance()
  const balancesWithValue = farmsWithBalance.filter((balanceType) => balanceType.balance.toNumber() > 0)

  const { onReward } = useAllHarvest(balancesWithValue.map((farmWithBalance) => farmWithBalance.pid))

  const harvestAllFarms = useCallback(async () => {
    setPendingTx(true)
    try {
      await onReward()
    } catch (error) {
      // TODO: find a way to handle when the user rejects transaction or it fails
    } finally {
      setPendingTx(false)
    }
  }, [onReward])

  const addWatchToxicToken = useCallback(async () => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    const provider = window.ethereum
    if (provider) {
      try {
        // wasAdded is a boolean. Like any RPC method, an error may be thrown.
        const wasAdded = await provider.request({
          method: 'wallet_watchAsset',
          params: {
            type: 'ERC20',
            options: {
              address: '0x6e3D93E8D026934D7F230900F0d1B35F6CB00347',
              symbol: 'TOXIC',
              decimals: '18',
              image:
                'https://bitbucket.org/toxicfinance/front/raw/137c7529c2ae932a1d769df59f8aeb937447729b/public/images/farms/toxic.png',
            },
          },
        })

        if (wasAdded) {
          console.log('Token was added')
        }
      } catch (error) {
        // TODO: find a way to handle when the user rejects transaction or it fails
      }
    }
  }, [])

  return (
    <StyledFarmStakingCard>
      <CardBody>
        <Block>
        <TokenImageWrapper>
        <CardImage
            src="https://bitbucket.org/toxicfinance/front/raw/137c7529c2ae932a1d769df59f8aeb937447729b/public/images/toxic/2.png"
            alt="toxic logo"
            width={64}
            height={64}
          />
          <ToxicHarvestBalance />
          </TokenImageWrapper>
          
          <Button onClick={addWatchToxicToken} scale="sm">
            <Text bold color="invertedContrast" fontSize="16px">+{' '}</Text>
            <img
              style={{ marginLeft: 8 }}
              width={16}
              src="https://bitbucket.org/toxicfinance/front/raw/137c7529c2ae932a1d769df59f8aeb937447729b/public/images/wallet/metamask.png"
              alt="metamask logo"
            />
          </Button>
          <Label>{TranslateString(544, 'TOXIC to Harvest')}</Label>
          
        </Block>
        <Divider />
        <Block>
        <TokenImageWrapper>
        <CardImage
            src="https://bitbucket.org/toxicfinance/front/raw/137c7529c2ae932a1d769df59f8aeb937447729b/public/images/toxic/2.png"
            alt="toxic logo"
            width={64}
            height={64}
          />
          <ToxicWalletBalance />
           </TokenImageWrapper>
          <Label>{TranslateString(546, 'TOXIC in Wallet')}</Label>
          
        </Block>
        <Actions>
          {account ? (
            <Button id="harvest-all" disabled={balancesWithValue.length <= 0 || pendingTx} onClick={harvestAllFarms}>
              <Text bold color="invertedContrast" fontSize="16px">{pendingTx
                ? TranslateString(548, 'Collecting TOXIC')
                : TranslateString(999, `Harvest all (${balancesWithValue.length})`)}</Text>
            </Button>
          ) : (
            <UnlockButton fullWidth />
          )}
        </Actions>
      </CardBody>
    </StyledFarmStakingCard>
  )
}

export default FarmedStakingCard

import { MenuEntry } from '@toxicfinance/uikit'

const config: MenuEntry[] = [
  {
    label: 'Home',
    icon: 'HomeIcon',
    href: '/',
  },
  {
    label: 'Trade',
    icon: 'TradeIcon',
    items: [
      {
        label: 'Exchange',
        href: 'https://exchange.pancakeswap.finance/#/swap?outputCurrency=0x6e3D93E8D026934D7F230900F0d1B35F6CB00347',
        external: true,
      },
      {
        label: 'Liquidity',
        href: 'https://exchange.pancakeswap.finance/#/add/BNB/0x6e3D93E8D026934D7F230900F0d1B35F6CB00347',
        external: true,
      },
    ],
  },
  {
    label: 'Farms',
    icon: 'FarmIcon',
    href: '/farms',
  },
  {
    label: 'Nuclear',
    icon: 'NuclearIcon',
    href: '/nuclear',
  },
 {
    label: 'Info',
    icon: 'InfoIcon',
    items: [
      {
        label: 'PancakeSwap',
        href: 'https://pancakeswap.info/token/0x6e3D93E8D026934D7F230900F0d1B35F6CB00347',
        external: true,
      },
    ],
  },
  {
    label: 'Docs',
    icon: 'GitbookIcon',
    href: 'https://docs.toxic.finance',
    external: true,
  },
  {
    label: 'Audit',
    icon: 'AuditIcon',
    external: true,
    href: 'https://drive.google.com/file/d/1eLVXo47MAwtwIDamC5wNlefiPFl8lMim/view?usp=sharing',
  },
]

export default config

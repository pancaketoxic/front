import React from 'react'
import styled from 'styled-components'
import { Card, CardBody, Heading } from '@toxicfinance/uikit'
import useI18n from 'hooks/useI18n'
// import { useGetStats } from 'hooks/api'
import { useTotalValue } from '../../../state/hooks'
import CardValue from './CardValue'

const StyledTotalValueLockedCard = styled(Card)`
  padding: 8px;
  ${({ theme }) => theme.mediaQueries.sm} {
    padding: 16px;
  }
`
const WrapperRes = styled.div`
  display: flex;
  align-items: flex-start;
  flex-direction: column;
  ${({ theme }) => theme.mediaQueries.lg} {
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
  }
`

const TotalValueLockedCard = () => {
  const TranslateString = useI18n()
  // const data = useGetStats()
  const totalValue = useTotalValue()
  // const tvl = totalValue.toFixed(2);

  return (
    <StyledTotalValueLockedCard>
      <CardBody>
        <WrapperRes>
          <div>
            <Heading fontSize="56px" color="primary">
              {TranslateString(999, 'TVL')}
            </Heading>
            
          </div>
        
            <CardValue value={totalValue.toNumber()} prefix="$" decimals={2} fontSize="56px" color="primary" />
          
        </WrapperRes>
      </CardBody>
    </StyledTotalValueLockedCard>
  )
}

export default TotalValueLockedCard
